package cz.cvut.fel.swa.courseservice.repository;

import cz.cvut.fel.swa.courseservice.entity.Course;
import cz.cvut.fel.swa.courseservice.entity.Parallel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ParallelRepository extends CrudRepository<Parallel, Long> {
    List<Parallel> getAllParallels();

    Optional<Parallel> getParallelById(Long id);

    List<Parallel> getParallelsByCourse(Course course);
}
