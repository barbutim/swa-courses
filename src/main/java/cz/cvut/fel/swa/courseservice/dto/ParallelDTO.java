package cz.cvut.fel.swa.courseservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.cvut.fel.swa.courseservice.entity.Course;
import cz.cvut.fel.swa.courseservice.enums.ParallelType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDate;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ParallelDTO {
    @Schema(hidden = true)
    private Long id;

    private String teacher;

    private Course course;

    private ParallelType type;

    private LocalDate from;

    private LocalDate to;
}
