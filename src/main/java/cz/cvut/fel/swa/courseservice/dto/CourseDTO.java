package cz.cvut.fel.swa.courseservice.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import cz.cvut.fel.swa.courseservice.entity.Parallel;
import cz.cvut.fel.swa.courseservice.enums.CompletionType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Collection;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CourseDTO {
    @Schema(hidden = true)
    private Long id;

    private String code;

    private String name;

    private CompletionType completion;

    private Integer credits;

    private Collection<Parallel> parallels;
}
