package cz.cvut.fel.swa.courseservice.controller;

import cz.cvut.fel.swa.courseservice.dto.CourseDTO;
import cz.cvut.fel.swa.courseservice.entity.Course;
import cz.cvut.fel.swa.courseservice.mapper.CourseMapper;
import cz.cvut.fel.swa.courseservice.service.CourseService;
import org.hibernate.mapping.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    CourseService courseService;

    @Autowired
    CourseMapper courseMapper;

    @PutMapping
    public ResponseEntity<CourseDTO> createCourse(@RequestBody CourseDTO createDTO) {
        Course course = courseService.createCourse(createDTO);
        return new ResponseEntity<>(courseMapper.toDto(course), HttpStatus.CREATED);
    }

    @PostMapping("/{id}")
    public ResponseEntity<CourseDTO> updateCourse(@PathVariable Long id, @RequestBody CourseDTO updateDTO) {
        Course course = courseService.updateCourse(id, updateDTO);
        return new ResponseEntity<>(courseMapper.toDto(course), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Any> deleteCourse(@PathVariable Long id) {
        courseService.deleteCourse(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CourseDTO> getCourse(@PathVariable Long id) {
        Course course = courseService.getCourse(id);
        return new ResponseEntity<>(courseMapper.toDto(course), HttpStatus.FOUND);
    }

    @GetMapping("/{code}")
    public ResponseEntity<CourseDTO> getCourse(@PathVariable String code) {
        Course course = courseService.getCourse(code);
        return new ResponseEntity<>(courseMapper.toDto(course), HttpStatus.FOUND);
    }

    @GetMapping("/courses")
    public ResponseEntity<Collection<CourseDTO>> getAllCourses() {
        Collection<Course> courses = courseService.getAllCourses();
        return new ResponseEntity<>(courseMapper.manyToDto(courses), HttpStatus.FOUND);
    }

    @GetMapping("/{credits}/courses")
    public ResponseEntity<Collection<CourseDTO>> getCoursesByCredits(@PathVariable Integer credits) {
        Collection<Course> courses = courseService.getCoursesByCredits(credits);
        return new ResponseEntity<>(courseMapper.manyToDto(courses), HttpStatus.FOUND);
    }
}
