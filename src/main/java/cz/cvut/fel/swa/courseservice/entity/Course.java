package cz.cvut.fel.swa.courseservice.entity;

import cz.cvut.fel.swa.courseservice.enums.CompletionType;
import jakarta.persistence.*;
import lombok.Data;
import java.io.Serializable;
import java.util.Collection;

@Data
@Entity
@Table(name = "Course")
public class Course implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    public String code;

    public String name;

    public CompletionType completion;

    public Integer credits;

    @OneToMany(mappedBy = "course")
    public Collection<Parallel> parallels;
}
