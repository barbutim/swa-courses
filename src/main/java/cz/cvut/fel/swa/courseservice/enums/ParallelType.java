package cz.cvut.fel.swa.courseservice.enums;

public enum ParallelType {
    LECTURE,
    EXERCISE,
    LABORATORY
}
