package cz.cvut.fel.swa.courseservice.service;

import cz.cvut.fel.swa.courseservice.dto.CourseDTO;
import cz.cvut.fel.swa.courseservice.entity.Course;
import cz.cvut.fel.swa.courseservice.exception.NotFoundException;
import cz.cvut.fel.swa.courseservice.mapper.CourseMapper;
import cz.cvut.fel.swa.courseservice.repository.CourseRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CourseService {
    CourseRepository courseRepository;
    CourseMapper courseMapper;

    @Autowired
    public CourseService(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    public Course createCourse(CourseDTO courseDto){
        Course course = courseMapper.fromDto(courseDto);
        return courseRepository.save(course);
    }

    public Course updateCourse(Long id, CourseDTO courseDTO) {
        Course course = getCourse(id);
        course.code = courseDTO.getCode();
        course.name = courseDTO.getName();
        course.completion = courseDTO.getCompletion();
        course.credits = courseDTO.getCredits();
        course.parallels = courseDTO.getParallels();

        return courseRepository.save(course);
    }

    public void deleteCourse(Long id) {
        courseRepository.deleteById(id);
    }

    public Course getCourse(Long id) {
        return courseRepository.getCourseById(id)
                .orElseThrow(() -> new NotFoundException("Course ID \"" + id + "\" not found"));
    }

    public Course getCourse(String code) {
        return courseRepository.getCourseByCode(code)
                .orElseThrow(() -> new NotFoundException("Course CODE \"" + code + "\" not found"));
    }

    public Collection<Course> getAllCourses() {
        return courseRepository.getAllCourses();
    }

    public Collection<Course> getCoursesByCredits(Integer credits) {
        return courseRepository.getCoursesByCredits(credits);
    }

}
