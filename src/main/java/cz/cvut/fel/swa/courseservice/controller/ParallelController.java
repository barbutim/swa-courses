package cz.cvut.fel.swa.courseservice.controller;

import cz.cvut.fel.swa.courseservice.dto.ParallelDTO;
import cz.cvut.fel.swa.courseservice.entity.Course;
import cz.cvut.fel.swa.courseservice.entity.Parallel;
import cz.cvut.fel.swa.courseservice.mapper.ParallelMapper;
import cz.cvut.fel.swa.courseservice.service.ParallelService;
import org.hibernate.mapping.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
public class ParallelController {

    @Autowired
    ParallelService parallelService;

    @Autowired
    ParallelMapper parallelMapper;

    @PutMapping
    public ResponseEntity<ParallelDTO> createParallel(@RequestBody ParallelDTO createDTO) {
        Parallel parallel = parallelService.createParallel(createDTO);
        return new ResponseEntity<>(parallelMapper.toDto(parallel), HttpStatus.CREATED);
    }

    @PostMapping("/{id}")
    public ResponseEntity<ParallelDTO> updateParallel(@PathVariable Long id, @RequestBody ParallelDTO updateDTO) {
        Parallel parallel = parallelService.updateParallel(id, updateDTO);
        return new ResponseEntity<>(parallelMapper.toDto(parallel), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Any> deleteParallel(@PathVariable Long id) {
        parallelService.deleteParallel(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ParallelDTO> getParallel(@PathVariable Long id) {
        Parallel parallel = parallelService.getParallel(id);
        return new ResponseEntity<>(parallelMapper.toDto(parallel), HttpStatus.FOUND);
    }

    @GetMapping("/parallels")
    public ResponseEntity<Collection<ParallelDTO>> getAllParallels() {
        Collection<Parallel> parallels = parallelService.getAllParallels();
        return new ResponseEntity<>(parallelMapper.manyToDto(parallels), HttpStatus.FOUND);
    }

    @GetMapping("/{course}/parallels")
    public ResponseEntity<Collection<ParallelDTO>> getParallelsByCourse(@PathVariable Course course) {
        Collection<Parallel> parallels = parallelService.getParallelsByCourse(course);
        return new ResponseEntity<>(parallelMapper.manyToDto(parallels), HttpStatus.FOUND);
    }
}
