package cz.cvut.fel.swa.courseservice.mapper;

import cz.cvut.fel.swa.courseservice.dto.ParallelDTO;
import cz.cvut.fel.swa.courseservice.entity.Parallel;
import org.mapstruct.Mapper;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

@Mapper
public interface ParallelMapper extends CrudRepository<Parallel, Long> {
    Parallel fromDto(ParallelDTO parallelDTO);

    ParallelDTO toDto(Parallel parallel);

    Collection<ParallelDTO> manyToDto(Collection<Parallel> parallels);
}
