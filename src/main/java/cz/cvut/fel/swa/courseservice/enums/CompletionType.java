package cz.cvut.fel.swa.courseservice.enums;

public enum CompletionType {
    AE, // Assessment and examination
    A, // Assessment
    GA // Graded assessment
}
