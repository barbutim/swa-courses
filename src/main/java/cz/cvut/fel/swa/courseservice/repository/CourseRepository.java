package cz.cvut.fel.swa.courseservice.repository;

import cz.cvut.fel.swa.courseservice.entity.Course;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long> {
    @Query("SELECT c FROM Course c")
    List<Course> getAllCourses();

    Optional<Course> getCourseById(long id);

    Optional<Course> getCourseByCode(String code);

    List<Course> getCoursesByCredits(Integer credits);
}
