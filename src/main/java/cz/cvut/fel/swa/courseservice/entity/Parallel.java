package cz.cvut.fel.swa.courseservice.entity;

import cz.cvut.fel.swa.courseservice.enums.ParallelType;
import jakarta.persistence.*;
import lombok.Data;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "Parallel")
public class Parallel implements Serializable {
    @Id
    @GeneratedValue
    private Long id;

    public String teacher;

    public Course course;

    public ParallelType type;

    public LocalDate from;

    public LocalDate to;
}
