package cz.cvut.fel.swa.courseservice.service;

import cz.cvut.fel.swa.courseservice.dto.ParallelDTO;
import cz.cvut.fel.swa.courseservice.entity.Course;
import cz.cvut.fel.swa.courseservice.entity.Parallel;
import cz.cvut.fel.swa.courseservice.exception.NotFoundException;
import cz.cvut.fel.swa.courseservice.mapper.ParallelMapper;
import cz.cvut.fel.swa.courseservice.repository.ParallelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class ParallelService {
    ParallelRepository parallelRepository;
    ParallelMapper parallelMapper;

    @Autowired
    public ParallelService(ParallelRepository parallelRepository) {
        this.parallelRepository = parallelRepository;
    }

    public Parallel createParallel(ParallelDTO parallelDTO){
        Parallel parallel = parallelMapper.fromDto(parallelDTO);
        return parallelRepository.save(parallel);
    }

    public Parallel updateParallel(Long id, ParallelDTO parallelDTO) {
        Parallel parallel = getParallel(id);
        parallel.teacher = parallelDTO.getTeacher();
        parallel.course = parallelDTO.getCourse();
        parallel.type = parallelDTO.getType();
        parallel.from = parallelDTO.getFrom();
        parallel.to = parallelDTO.getTo();

        return parallelRepository.save(parallel);
    }

    public void deleteParallel(Long id) {
        parallelRepository.deleteById(id);
    }

    public Parallel getParallel(Long id) {
        return parallelRepository.getParallelById(id)
                .orElseThrow(() -> new NotFoundException("Parallel ID \"" + id + "\" not found"));
    }

    public Collection<Parallel> getAllParallels() {
        return parallelRepository.getAllParallels();
    }

    public Collection<Parallel> getParallelsByCourse(Course course) {
        return parallelRepository.getParallelsByCourse(course);
    }
}
