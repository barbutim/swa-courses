package cz.cvut.fel.swa.courseservice.mapper;

import cz.cvut.fel.swa.courseservice.dto.CourseDTO;
import cz.cvut.fel.swa.courseservice.entity.Course;
import org.mapstruct.Mapper;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

@Mapper
public interface CourseMapper extends CrudRepository<Course, Long> {
    Course fromDto(CourseDTO courseDto);

    CourseDTO toDto(Course course);

    Collection<CourseDTO> manyToDto(Collection<Course> courses);
}
